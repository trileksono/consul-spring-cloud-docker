package com.example.tri.oatuh2.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.approval.UserApprovalHandler;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeServices;
import org.springframework.security.oauth2.provider.code.JdbcAuthorizationCodeServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

@Configuration
public class Oauth2Configuration {

  @Configuration
  @EnableAuthorizationServer
  protected class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private DataSourceConfiguration mDataSourceConfiguration;

    @Autowired
    private UsersDetailService userDetailsService;

    @Autowired
    private BCryptPasswordEncoder mBCryptPasswordEncoder;

    @Bean
    public AuthorizationCodeServices authorizationCodeServices() {
      return new JdbcAuthorizationCodeServices(mDataSourceConfiguration.mDataSource());
    }

    @Bean
    public JwtAccessTokenConverter mJwtAccessTokenConverter() {
      KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(new ClassPathResource("jwt.jks"), "tleksono".toCharArray());
      JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
      converter.setKeyPair(keyStoreKeyFactory.getKeyPair("tleksono"));
      return converter;
    }

    @Bean
    public TokenStore tokenStore() {
      return new JwtTokenStore(mJwtAccessTokenConverter());
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
      endpoints
              .tokenStore(tokenStore())
              .accessTokenConverter(mJwtAccessTokenConverter())
              .authorizationCodeServices(authorizationCodeServices())
              .authenticationManager(authenticationManager)
              .userDetailsService(userDetailsService).approvalStoreDisabled();
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
      clients.jdbc(mDataSourceConfiguration.mDataSource());
    }
  }

  @Configuration
  @EnableResourceServer
  protected class ResourceServer extends ResourceServerConfigurerAdapter {

    private static final String RESOURCE_ID = "PEMINJAMAN";

    @Autowired
    private TokenStore mTokenStore;

    @Override
    public void configure(HttpSecurity http) throws Exception {
      http.authorizeRequests()
              .antMatchers("/my-health-check").permitAll()
              .antMatchers("/**")
              .authenticated();
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
      resources.resourceId(RESOURCE_ID).tokenStore(mTokenStore);
    }
  }
}
