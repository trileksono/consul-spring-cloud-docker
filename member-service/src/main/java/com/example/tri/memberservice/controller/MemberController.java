package com.example.tri.memberservice.controller;

import com.example.tri.memberservice.entity.Member;
import com.example.tri.memberservice.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/member")
public class MemberController {

  @Autowired
  private MemberService mMemberService;

  @GetMapping("/{id}")
  public Member getById(@PathVariable("id") Member member) {
    return member;
  }

  @PutMapping("/{id}")
  public Member update(@PathVariable("id") Member member, @Valid @RequestBody Member memberUpdate) {
    return mMemberService.update(member, memberUpdate);
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Member create(@Valid @RequestBody Member member) {
    return mMemberService.save(member);
  }

  @DeleteMapping
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delete(@RequestBody Long id) {
    mMemberService.delete(id);
  }

  @GetMapping
  public Page<Member> findAll(Pageable pageable) {
    return mMemberService.findAll(pageable);
  }

}
