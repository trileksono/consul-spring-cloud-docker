package com.example.tri.memberservice.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "member")
@Data
public class Member {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column( name = "id_member")
  private Long idMember;

  @Column(name = "nama")
  @Size(min = 3, max = 200)
  private String nama;

  @Column(name = "alamat")
  @NotNull
  private String alamat;

}
