package com.example.tri.transactionservice.service;

import com.example.tri.transactionservice.model.Book;
import com.example.tri.transactionservice.service.impl.BookServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "book-service", fallback = BookServiceImpl.class)
public interface BookService {

  @GetMapping(value = "/book/{id}")
  Book getById(@PathVariable("id") Long id);
}
