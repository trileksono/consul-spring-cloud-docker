package com.example.tri.transactionservice.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Member {
  private Long idMember;
  private String nama;
  private String alamat;
}
