CREATE TABLE IF NOT EXISTS transactions (
	id varchar(255) NOT NULL,
	id_buku bigint NOT NULL,
	id_member bigint NOT NULL,
	tgl_peminjaman date NULL,
	tgl_pengembalian date NULL,
	CONSTRAINT pk PRIMARY KEY (id)
);