# CONSUL CENTRALIZATION CONFIG

## RUNNING ON LINUX

* Pastikan sudah install docker dan docker compose pada os anda
* Jalankan file `sh build-all.sh` untuk build docker image
* Jalankan `docker-compose up` untuk menjalankan semua container

### Cara Mengakses 

* grant type : client_credentials

```bash
curl -X POST \
  http://localhost:8080/oauth/token \
  -H 'Authorization: Basic Y2xpZW50aWQ6MTIzNA==' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -d grant_type=client_credentials
 ```
   * response
 
 ```json
 {
    "access_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiUEVNSU5KQU1BTiJdLCJzY29wZSI6WyJyZWFkIiwid3JpdGUiXSwiZXhwIjoxNTU0Mzg0OTMyLCJhdXRob3JpdGllcyI6WyJDTElFTlQiXSwianRpIjoiNTVhNzk5NmMtMjYzOC00ZTc1LThlYTYtZmMwOWYwNGExMzQyIiwiY2xpZW50X2lkIjoiY2xpZW50aWQifQ.W5z4rjri4WhHn9OszF8aDH5lwvWy4khusJQ-0qhirk94IMMXnDTWrIj-II-9SflfehQFbK55u3BfcPYUD6E6KmaT4J96MVfW9ZPXqG2XhkBjhZUpphHcQjqYSLl1DU5gwcjxGRPTGeLzeEraaoaXUS2kONXroiBtk0rxn9qAygy6UQfXYJu0NWoezgJqWCDCtldu4UdAzLZYuZ_o5yovJaslR05jHUugvC38jZ9NW-K6a6ngstlMjN7iHcDlgmBrM3TVVkrI98x8z5q1nCMKITlBhBLf0jgA9V_Z3npF7h37mI3JACRmbnAtRVS_bMDeIrNZcyWdZwONdd8FCN1rjg",
    "token_type": "bearer",
    "expires_in": 3599,
    "scope": "read write",
    "jti": "55a7996c-2638-4e75-8ea6-fc09f04a1342"
}
```

* grant type : password

```bash
curl -X POST \
  http://localhost:8080/oauth/token \
  -H 'Authorization: Basic Y2xpZW50aWQ6MTIzNA==' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -d 'grant_type=password&username=tri&password=1234'
 ```
 
*  response 

```json
{
    "access_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiUEVNSU5KQU1BTiJdLCJ1c2VyX25hbWUiOiJ0cmkiLCJzY29wZSI6WyJyZWFkIiwid3JpdGUiXSwiZXhwIjoxNTU0Mzg1MTI3LCJhdXRob3JpdGllcyI6WyJBRE1JTiJdLCJqdGkiOiI5ODdkNDVjYy1kZTRhLTRlZTYtYmJkOC0yZTM1NDk2Y2NhYTUiLCJjbGllbnRfaWQiOiJjbGllbnRpZCJ9.fy8OdaYER_JOVgNWpeX2hwkJrGYWYFGgee-vxZ8Ggc6BdBIO3z4TMzLpaHlbm1tuxZHmigNUDZvv3A5hx5JAIumPeZ21WCVVPFz58PhYKxZfa7JM-LgKEdt3q0-du-y5tfmB-KqTKgWX5DHGGdGWSAdcUSBue_HG0upwrc0GOxkGC_qq-aa5I88_-bT3d2qIkrnmDcsBc3wTjrVJSrRF17P9YwecuRyE47x5ZyihSx9lBIcv6ohBym1wazsPz8pzDzX_-Xf5vfP2PSvaKeBLuxEKpa7SB-b_LO7mVkppzdRAnJJZti9ax__iRMRKu3dBQTHxue2kS1SJtZTBXREt_g",
    "token_type": "bearer",
    "refresh_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiUEVNSU5KQU1BTiJdLCJ1c2VyX25hbWUiOiJ0cmkiLCJzY29wZSI6WyJyZWFkIiwid3JpdGUiXSwiYXRpIjoiOTg3ZDQ1Y2MtZGU0YS00ZWU2LWJiZDgtMmUzNTQ5NmNjYWE1IiwiZXhwIjoxNTU2OTczNTI3LCJhdXRob3JpdGllcyI6WyJBRE1JTiJdLCJqdGkiOiI3Y2ZhZTc3OS01NTgyLTRlNTYtOTE2NS03Y2I1OGNlMTZlMDciLCJjbGllbnRfaWQiOiJjbGllbnRpZCJ9.ftb5_umzf7S_ZpaQa9PG2f4Vi7vwUeVL2pL6-KL8e476kyoZeKqrsBlO9VCzVrx0eGUybjBYyotuIfKjD7RXr80E6F2nT2liUpu1veMD15ADhJVopJFyBWEL-tMZGDk04spLOi1hJcLJiU5a4x8SqaThEyJ4x3ssyQG43xXdEqv90V8M1-RnR2gunklLRza0rLAyPKX7RHzIiRYZnFlwwYMxbxliXdmvAe-_BHTiTIgRCCez5wYxzvO3G4h1T1yfTGyBxfUfh-ufiBcBaWvFPJZF-_3rE1W2mrOGPTFhQVy1HENjBNjgnJFtyNILxVq1EbqBEzXhnzdR74D0F6rGbw",
    "expires_in": 3599,
    "scope": "read write",
    "jti": "987d45cc-de4a-4ee6-bbd8-2e35496ccaa5"
}
```
 
 
### Gunakan access token untuk mengakses data

* book service

```bash
curl -X GET \
  http://localhost:8080/book \
  -H 'Authorization: bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiUEVNSU5KQU1BTiJdLCJ1c2VyX25hbWUiOiJ0cmkiLCJzY29wZSI6WyJyZWFkIiwid3JpdGUiXSwiZXhwIjoxNTU0Mzg1MTI3LCJhdXRob3JpdGllcyI6WyJBRE1JTiJdLCJqdGkiOiI5ODdkNDVjYy1kZTRhLTRlZTYtYmJkOC0yZTM1NDk2Y2NhYTUiLCJjbGllbnRfaWQiOiJjbGllbnRpZCJ9.fy8OdaYER_JOVgNWpeX2hwkJrGYWYFGgee-vxZ8Ggc6BdBIO3z4TMzLpaHlbm1tuxZHmigNUDZvv3A5hx5JAIumPeZ21WCVVPFz58PhYKxZfa7JM-LgKEdt3q0-du-y5tfmB-KqTKgWX5DHGGdGWSAdcUSBue_HG0upwrc0GOxkGC_qq-aa5I88_-bT3d2qIkrnmDcsBc3wTjrVJSrRF17P9YwecuRyE47x5ZyihSx9lBIcv6ohBym1wazsPz8pzDzX_-Xf5vfP2PSvaKeBLuxEKpa7SB-b_LO7mVkppzdRAnJJZti9ax__iRMRKu3dBQTHxue2kS1SJtZTBXREt_g' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -H 'Postman-Token: cb18764e-cc80-4d45-ab62-818bc08b103c'
```

* response

```json
{
    "content": [
        {
            "idBook": 1,
            "namaBuku": "Walking with Abel",
            "pengarang": "Anna Badkhen"
        },
        {
            "idBook": 2,
            "namaBuku": "Fortune Smiles",
            "pengarang": "Adam Johnson"
        },
        {
            "idBook": 3,
            "namaBuku": "Pedigree",
            "pengarang": "Patrick Modiano"
        },
        {
            "idBook": 4,
            "namaBuku": "Dear Mister Essay Writer Guy",
            "pengarang": "Dinty Moore"
        },
        {
            "idBook": 5,
            "namaBuku": "Eileen",
            "pengarang": "Ottessa Moshfegh"
        }
    ],
    "pageable": {
        "sort": {
            "sorted": false,
            "unsorted": true,
            "empty": true
        },
        "pageSize": 20,
        "pageNumber": 0,
        "offset": 0,
        "unpaged": false,
        "paged": true
    },
    "totalPages": 1,
    "totalElements": 5,
    "last": true,
    "numberOfElements": 5,
    "sort": {
        "sorted": false,
        "unsorted": true,
        "empty": true
    },
    "first": true,
    "size": 20,
    "number": 0,
    "empty": false
}
```

* Consul status node

![consul img](https://gitlab.com/trileksono/consul-spring-cloud-docker/raw/master/image/consul.png)

* Consul centralization config

![consul img](https://gitlab.com/trileksono/consul-spring-cloud-docker/raw/master/image/consul-config.png)
