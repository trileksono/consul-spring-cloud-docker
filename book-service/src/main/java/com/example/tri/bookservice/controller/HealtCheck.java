package com.example.tri.bookservice.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealtCheck {

  @GetMapping("/book/my-health-check")
  private ResponseEntity healthCheck() {
    return new ResponseEntity(HttpStatus.OK);
  }
}
