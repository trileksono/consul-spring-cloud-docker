package com.example.tri.bookservice.service.impl;

import com.example.tri.bookservice.entity.Book;
import com.example.tri.bookservice.repo.BookRepository;
import com.example.tri.bookservice.service.BookService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
public class BookServiceImpl implements BookService {

  @Autowired
  private BookRepository mBookRepository;

  @Override
  public Book save(Book book) {
    return mBookRepository.save(book);
  }

  @Override
  public void delete(Long id) {
    mBookRepository.deleteById(id);
  }

  @Override
  public Page<Book> findAll(Pageable pageable) {
    return mBookRepository.findAll(pageable);
  }

  @Override
  public Book update(Book book, Book bookUpdate) {
    if (book == null) {
      throw new EntityNotFoundException();
    }
    bookUpdate.setIdBook(book.getIdBook());
    BeanUtils.copyProperties(bookUpdate, book);
    return mBookRepository.save(book);
  }
}
