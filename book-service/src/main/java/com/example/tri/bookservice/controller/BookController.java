package com.example.tri.bookservice.controller;

import com.example.tri.bookservice.entity.Book;
import com.example.tri.bookservice.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/book")
public class BookController {

  @Autowired
  private BookService mBookService;

  @GetMapping("/{id}")
  public Book getById(@PathVariable("id") Book book) {
    return book;
  }

  @PutMapping("/{id}")
  public Book update(@PathVariable("id") Book book, @Valid @RequestBody Book bookUpdate) {
    return mBookService.update(book, bookUpdate);
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Book create(@Valid @RequestBody Book book) {
    return mBookService.save(book);
  }

  @DeleteMapping
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delete(@RequestBody Long id) {
    mBookService.delete(id);
  }

  @GetMapping
  public Page<Book> findAll(Pageable pageable) {
    return mBookService.findAll(pageable);
  }
}
