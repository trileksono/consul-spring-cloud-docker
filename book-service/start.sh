#!/usr/bin/env bash

./wait-for-it.sh consul:8500 -t 60 -- java -Djava.security.egd=file:/dev/./urandom -jar app.jar