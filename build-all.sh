#!/bin/bash

function progress() {
    local GREEN CLEAN
    GREEN='\033[0;32m'
    CLEAN='\033[0m'
    printf "\n${GREEN}$@  ${CLEAN}\n" >&2
}

set -e

# Docker image prefix
REGPREFIX=tleksono

cd book-service
mvn clean package -DskipTests
progress "Building book service image ..."
docker build -t ${REGPREFIX}/book -q .
cd -

cd member-service
mvn clean package -DskipTests
progress "Building member service image ..."
docker build -t ${REGPREFIX}/member -q .
cd -

cd oatuh2
mvn clean package -DskipTests
progress "Building oauth gateway image ..."
docker build -t ${REGPREFIX}/oauth-gateway -q .
cd -

cd transaction-service
mvn clean package -DskipTests
progress "Building transaction service image ..."
docker build -t ${REGPREFIX}/transactions -q .
cd -

progress "Building remove none tag images ..."
docker rmi $(docker images --filter 'dangling=true' -q --no-trunc) -f
